"use strict";

// ## Теоретичні питання

// 1. Опишіть своїми словами, що таке метод об'єкту.

// Функції, які знаходяться всередині об'єкту і є його властивостями, називають методами об'єкту.

// 2. Який тип даних може мати значення властивості об'єкта?

// Значення властивостей об'єкта можуть мати примітивні або об'єктні типи данних.

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// Означає, що змінна, якій присвоєнно об'єкт, має в собі не сам об'єкт, а посилання на місце в пам'яті, де зберігається цей об'єкт.

// ## Задание

// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:

// - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.

function createNewUser() {
  let userFirstName = prompt("Enter your name");
  let userLastName = prompt("Enter your lastname");

  const newUser = {};

  newUser["firstName"] = userFirstName;
  newUser["lastName"] = userLastName;
  newUser["getLogin"] = function () {
    return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase();
  };

  // Object.defineProperty(newUser, "firstName", { writable: false, });
  // Object.defineProperty(newUser, "lastName", { writable: false, });

  // function setFirstName() {
  //   if (newUser["firstName"] === userFirstName) {
  //     Object.defineProperty(newUser, "firstName", {
  //       writable: true,
  //     });
  //   }
  // }
  // setFirstName()

  // function setLastName() {
  //   if (newUser["lastName"] === userLastName) {
  //     Object.defineProperty(newUser, "lastName", {
  //       writable: true,
  //     });
  //   }
  // }
  // setLastName()

  // newUser["firstName"] = "Viktor";
  // newUser["lastName"] = "Melnik";

  console.log(newUser.getLogin());
}

createNewUser();
